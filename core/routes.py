from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class FormProtection(object):
    @classmethod
    def begin(cls):
        return CSRFProtect()


class HTMLContent_form(FlaskForm):
    html_content_function = StringField('Table Name', validators=[DataRequired()])
    html_id = StringField('Associated CSS')
    html_content_code = SelectField('Type of Function', [DataRequired()], choices=[('Menu', ' add to menu'),
                                                                                   ('Title', 'Page Title'),
                                                                                   ('Pokemon Trainer', 'pokemon')])
    html_style = StringField('?')
    url_page_name = StringField('What\'s my page url?')
