from flask import Flask, render_template, request, url_for, redirect, jsonify, Markup, escape
from .models import TestSession, LogCreation, ActivityLog, HTMLContent, db
from .routes import HTMLContent_form, FormProtection

import random

''' FOLDER IMPORT '''
from .config import Configuration, Configurationdb

''' DEFINE APP '''
app = Flask(__name__)
FormProtection.begin().init_app(app)
app.config.from_object(Configurationdb)
app.config.from_object(Configuration)

# DEFINE Database Connection
with app.app_context():
    db.init_app(app)
    db.create_all()


# Custom functions or classes
def random_session():
    return random.randint(0, 1000000000)


@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")


# DEFINE Routes
@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        tb1 = TestSession(random_session())
        db.session.add(tb1)

        LogCreation(random_session(), f"TBL:{tb1} MSG:create")
        return redirect(url_for('query_each_db'))

    if request.method == 'GET':
        return render_template('index.html', t='Done.')


@app.route('/c')
@app.route('/c/<int:id>', methods=['GET', 'POST'])
def query_each_db(id=None):
    if id is None:
        return render_template('check.html', each=TestSession.query.all())

    if request.method == 'GET':
        return render_template('check.html', single=TestSession.query.filter_by(id=id).first())


@app.route('/c/del', methods=['POST'])
def del_each_entry():
    tb1 = TestSession.query.filter_by(id=request.form['data']).first()
    db.session.delete(tb1)
    db.session.commit()

    LogCreation(random_session(), f"TBL:{tb1} MSG:del")

    return jsonify({'output': 'Successful: <a href=\'{}\'>redirect</a> '.format(url_for('query_each_db'))})


@app.route('/logs', methods=['GET'])
def log_each():
    return render_template('check.html', y_each=ActivityLog.query.all())


@app.route('/page-creation')
@app.route('/page-creation/')
def html_page_creation():
    var = list()

    path_filtered = escape(request.path.replace("/", ""))
    [var.append([e.id, e.html_class]) for e in HTMLContent.query.filter_by(url_page_name=path_filtered)]
    v = f'{var[0][1]}{var[0][0]}'

    return render_template('create.html', var=v)
 #        return jsonify({'x': var[0]})


@app.route('/html-content')
@app.route('/html-content/')
@app.route('/html-content/<var>', methods=['GET', 'POST'])
def html_contents(var=''):
    site_content = {
        'title': 'html_content',
        'menu': Markup(f"<a href=\'{url_for('html_contents')}form\'>html_content</a>"),
    }
    #    title = "Html-content"
    form = HTMLContent_form()

    if var == 'content' and request.method == 'GET':
        return render_template('html_content.html', title="", c=site_content['menu'])

    elif var == 'form' and request.method == 'GET':
        title = "form-GET"
        hc = HTMLContent.query.all()
        title = [u.url_page_name for u in hc]

        # return jsonify({'users': title[1]})
        return render_template('html_content.html', title=title, form=form, hc=hc)

    elif var == 'u' and request.method == 'POST':
        qf = request.form
        a_string = HTMLContent(qf['html_content_function'], qf['html_id'], "", "", qf['url_page_name'])
        db.session.add(a_string)
        db.session.commit()
        LogCreation(random_session(), f"TBL:{a_string} MSG:{'create'}")

        title = "u-POST"
        return render_template('html_content.html', title=title, c=site_content['menu'])

    return render_template('html_content.html', title="Fail-End", c=var)


if __name__ == '__main__':
    pass
